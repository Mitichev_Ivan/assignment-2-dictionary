%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define MAX_SIZE 256
section .bss
    buff: resb MAX_SIZE
section .rodata
	err_message: db 'no key'
section .text
global _start
_start:
	mov rdi, buff
	mov rsi, 255
	push rdi
	push rsi
	call read_word
	pop rsi
	pop rdi
	test rax, rax
	je .error
	mov rdi, rax
	mov rsi, element
	call find_word
	test rax, rax
	je .error
	add rax, 8
	add rdx, rax
	mov rdi, rdx
	inc rdi
	call print_string
	jmp .end
	.error:
		mov rdi, err_message
		call print_err
	.end:
		xor rdi, rdi
		jmp exit






