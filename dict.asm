%include "lib.inc"

section .text
global find_word
find_word:
    xor rax,rax
    .loop:
	test rsi, rsi
	jz .fail
	push rdi
	push rsi
	add rsi, 8
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	jne .success
	mov rsi, [rsi]
	jmp .loop
    .success:
	mov rax, rsi
    .fail:
	ret

	
