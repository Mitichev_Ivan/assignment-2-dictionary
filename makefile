.PHONY: build clear rebuild
ASM=nasm
ASMFLAGS=-g -f elf64 -o
LD=ld -o
RM=rm -f
build: make_o
	$(LD) main main.o lib.o dict.o

make_o:
	$(ASM) $(ASMFLAGS) main.o main.asm
	$(ASM) $(ASMFLAGS) lib.o lib.asm
	$(ASM) $(ASMFLAGS) dict.o dict.asm
clear:
	$(RM) *.o
	$(RM) main

rebuild: clear build

test: 
	python test.py
