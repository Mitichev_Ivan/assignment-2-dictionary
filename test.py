# -*- coding: utf-8 -*-
import subprocess
import re
make = "make build"
main = "./main"
err = "no key"
label_list = [["wow", "first word explanation", ""],["kek", "second word explanation",""],["lol how", "third word explanation",""], ["TooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordTooLongWordToo", "", "no key"],["x", "","no key"], [" ", "","no key"],  ["\n", "","no key"], ["1"*256, "","no key"], ["1"*5000, "","no key"]]

def test(input_label, expected_stdout, expected_stderr):
	process = subprocess.Popen([main], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate(input=input_label)
	print("input:")
	print(input_label)
	print("stdout:")
	print(stdout.decode().strip())
	print("stderr:")
	print(stderr.decode().strip())
	if stdout != expected_stdout:
		print("Errors in stdout, expected "  + expected_stdout + " but got "+stdout )
	else:
		if not re.match(expected_stderr, stderr):
        		print("Errors in stderr, expected " + expected_stderr + " but got " + stderr)
	
		else:
        		print("All OK\n")

output = subprocess.check_output(make, shell=True)
print(output)

for i in range(1, len(label_list) + 1):
	test(label_list[i - 1][0], label_list[i - 1][1],label_list[i - 1][2])

